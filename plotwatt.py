__author__ = 'willr'

from plotwatt.aws import AWSTools

from fabric.api import *
from fabric.contrib.files import exists
"""
After AWS ASG deployed, run the host list and deploy new or updated software
"""
AWS_USER = 'ec2-user'
SSH_KEY_FILE = 'XXXXX.pem'

REPO = 'git@bitbucket.org/XXXX'
DEST = 'plotwatt'

def init():
    aws = AWSTools('us-east-1', 'plot-watt-asg')
    env.hosts = aws.get_instance_addresses()
    env.key_filename = SSH_KEY_FILE
    env.user = AWS_USER
    #fab.env.parallel = True
    env.use_ssh_config = True


def update_code():
    ''' Deploy new code on AWS autoscale instances'''
    if exists(DEST):
        with cd(DEST):
            run('git checkout master')
            run('git pull')
    else:
        run('git clone {} {}'.format(REPO, DEST))


def deploy():
    ''' Update codebase and restart daemon'''
    update_code()
    run('sudo python plotwattdaemon.py stop')
    run('sudo python plotwattdaemon.py start')


def main():
    init()
    print env.hosts
    #Tremendous hack to get around a bug in v1.8 Fabric
    for host in env.hosts:
        env.host_string = host
        run('uname -a')
        deploy()


if __name__ == '__main__':
    main()
