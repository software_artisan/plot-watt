from unittest import TestCase

__author__ = 'willr'

from plotwatt.jobcontrol import Job


class TestJob(TestCase):
    def test_to_json(self):
        j = Job(job_id='my_id', status='testing', crt_time='today', cmd='do_this', ouput_url='howdy')
        out = j.to_json()
        assert 'testing' in out
        assert 'today' in out
        assert '"job_id": "my_id"' in out

    def test_from_json(self):
        j = Job(job_id='my_id', status='testing', crt_time='today', cmd='do_this', ouput_url='howdy')
        k = Job().from_json(
            '{"status": "testing", "crt_time": "today", "cmd": "do_this", "job_id": "my_id", "output_url": "howdy"}')
        assert j.job_id == k.job_id
