from unittest import TestCase

__author__ = 'willr'
import os
from plotwatt.simple import FileJobResultStore


class TestFileJobResultStore(TestCase):
    def test_make_directory(self):
        fjs = FileJobResultStore('.')
        fjs.make_directory('test_directory_parent')

        assert os.path.isdir('./test_directory_parent')

    def test_store(self):
        fjs = FileJobResultStore('.')
        fjs.store('fake_job_id', 'contents unknown')

        assert os.path.exists('fake_job_id')

        fake_id, out = fjs.retrieve('fake_job_id')

        assert fake_id == 'fake_jobe_id'
        assert out == 'contents unknown'
