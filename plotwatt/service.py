__author__ = 'willr'

import logging

# From
# https://stackoverflow.com/questions/6553423/multiple-subprocesses-with-timeouts
import subprocess
import signal
import os
import threading
import errno
from contextlib import contextmanager

class TimeoutThread(object):
    def __init__(self, seconds):
        self.seconds = seconds
        self.cond = threading.Condition()
        self.cancelled = False
        self.thread = threading.Thread(target=self._wait)

    def run(self):
        """Begin the timeout."""

        self.thread.start()

    def _wait(self):
        with self.cond:
            self.cond.wait(self.seconds)

            if not self.cancelled:
                self.timed_out()

    def cancel(self):
        """Cancel the timeout, if it hasn't yet occured."""
        with self.cond:
            self.cancelled = True
            self.cond.notify()
        self.thread.join()

    def timed_out(self):
        """The timeout has expired."""
        raise NotImplementedError


class KillProcessThread(TimeoutThread):
    def __init__(self, seconds, pid):
        super(KillProcessThread, self).__init__(seconds)
        self.pid = pid

    def timed_out(self):
        try:
            os.kill(self.pid, signal.SIGINT)
        except OSError as e:
            # If the process is already gone, ignore the error.
            if e.errno not in (errno.EPERM, errno.ESRCH):
                raise e


@contextmanager
def process_timeout(seconds, pid):
    timeout = KillProcessThread(seconds, pid)
    timeout.run()
    try:
        yield
    finally:
        timeout.cancel()


import sys

ON_POSIX = 'posix' in sys.builtin_module_names


def run_command(job):
    result_code = 0
    out = None
    try:
        proc = subprocess.Popen(
            job.cmd, shell=True, stdin=None, stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
            close_fds=ON_POSIX)
        with process_timeout(1, proc.pid):
            out = proc.communicate()
        result_code = proc.wait()
    except Exception as ce:
        logging.error("Error running command %s %r", job, ce)

    return result_code, out

def process_jobs(message_service, store_service, tracking_service):

    for msg, job in message_service:

        if tracking_service.can_start(job.job_id):
            tracking_service.start_job(job_id=job.job_id,location=get_service_location())
            message_service.task_accepted(msg)

        result_code, out = run_command(job)

        if out is not None and len(out) > 0:
                job.output_url = store_service.store(job.job_id, out)

        if result_code != 0:
            tracking_service.fail_job(job_id=job.job_id,status_code=result_code,output_url=job.output_url)
        else:
            tracking_service.finish_job(job_id=job.job_id,status_code=result_code,output_url=job.output_url)

from .sqs import SQSMessageService
from .s3 import S3JobResultStore
from .sdb import SdbJobTracker

# Main
def service():
    message_service = SQSMessageService(region='us-east-1', domain_name='plotwatt_jobs')
    store_service = S3JobResultStore(region='us-east-1',    domain_name='plotwatt_jobs')
    tracking_service = SdbJobTracker(region='us-east-1',    domain_name='plotwatt_jobs')
    process_jobs(message_service, store_service, tracking_service)

def main():
    service()

def signal_handler(sig, frame):
    print("Shutting down PlotWatt job service.")
    exit(0)

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal_handler)
    main()

def get_service_location():
    import boto.utils

    try:
        meta = boto.utils.get_instance_metadata()

        if len(meta.keys()) > 0:
            return meta.get('public-hostname',None)

    except Exception as ex:
        pass

    import socket
    return socket.gethostname()