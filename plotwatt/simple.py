__author__ = 'willr'

import logging
from .jobcontrol import Job

# Simple versions


class SimpleTaskMessageService(object):
    def __init__(self, commands):
        self.commands = commands

    def __iter__(self):
        for command in self.commands:
            yield Job(cmd=command)

    def task_processed(self, job):
        logging.info("Processed task %s %s", job.job_id, job.task)


class SimpleJobTrackingService(object):
    def log(self, job, status):
        logging.info("Logging task %s %s %s", job.job_id, status, job.task)
        print("Logging task {} {} {}".format(job.job_id, status, job.task))


class SimpleJobResultStore(object):
    def store(self, job_id, asset):
        print("Storing asset {} \n{}".format(job_id, asset))


import os
import errno


class FileJobResultStore(SimpleJobResultStore):
    def __init__(self, base):
        self.base = base
        self.make_directory(base)

    def make_directory(self, base):
        try:
            os.makedirs(base)
        except OSError as exception:
            if exception.errno != errno.EEXIST:
                raise

    def store(self, job_id, asset):
        asset_path = os.path.join(self.base, str(job_id))
        f = open(asset_path, 'w')
        f.write(asset)
        f.close()

    def retrieve(self, job_id):
        asset_path = os.path.join(self.base, str(job_id))
        f = open(asset_path, 'w')
        out = f.readall()
        f.close()
        return job_id, out
