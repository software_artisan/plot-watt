__author__ = 'willr'

from plotwatt.jobcontrol import JobControl
from plotwatt.aws import AWSTools
from requests import get
import datetime
import time

class TestJobControl:

    def setup(self):
        self.job_control = JobControl(region='us-east-1')
        self.aws = AWSTools(region='us-east-1')

    def test_register_job(self):
        job = self.job_control.register_job(
            'uname -a;echo "VERY DISTINCTIVE OUTPUT {}"'.format(datetime.datetime.now().isoformat()))

        print 'Launching job '+job.job_id

        job2 = self.job_control.find_job(job.job_id)

        assert( job2 is not None )
        assert ( job.job_id == job2.job_id )
        assert ( job.cmd == job2.cmd )

    def __wait_until_finished(self,job):

        notFinished = True
        checks = 10
        while notFinished and checks > 0:
            time.sleep(5)
            job2 = self.job_control.find_job(job.job_id)
            assert( job2 is not None )
            print "Job {} status {}".format(job2.job_id,job2.status)
            notFinished = not ( job2.status == 'finished' )
            checks -= 1

        return checks >0 and not notFinished

    def test_begin_end_of_job(self):
        job = self.job_control.register_job(
            'uname -a;sleep 20;echo "VERY DISTINCTIVE OUTPUT {}"'.format(datetime.datetime.now().isoformat()))

        #Check that it was found
        assert self.__wait_until_finished(job)

        job2 = self.job_control.find_job(job.job_id)

        print get(job2.output_url)

    def test_failed_job(self):
        job = self.job_control.register_job(
            'uname -a;echo "VERY DISTINCTIVE OUTPUT {}";exit 33'.format(datetime.datetime.now().isoformat()))

        assert self.__wait_until_finished(job)

        job2 = self.job_control.find_job(job.job_id)

        print job2

        assert job2.status == 'failed'

    def test_job_dispersion(self):
        jobs = {}
        for i in range(10):
            job = self.job_control.register_job(
                'uname -a;sleep 20;echo "VERY DISTINCTIVE OUTPUT {}"'.format(datetime.datetime.now().isoformat()))
            assert ( job is not None )
            jobs.update( {i:job.job_id})

        for m in self.aws.get_instance_addresses():
            print "Instance: {}".format(m)

        ct = 0
        uniqueMachines = {}
        assignedMachines = {}
        for r in self.job_control.list_jobs():
            if r['job_id'] in jobs.values():
                ct += 1
                uniqueMachines.update({r['location']:1})
                assignedMachines.update({r['job_id']:r['location']})

        print "Unique machines"
        for k in uniqueMachines.keys():
            print k + ":" + uniqueMachines[k]

        print "Assigned machines"
        for k in assignedMachines.keys():
            print k + ":" + assignedMachines[k]

        #Check to see if distributed to more than one machine
        assert len(uniqueMachines.keys()) > 1

