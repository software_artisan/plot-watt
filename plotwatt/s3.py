__author__ = 'willr'

import boto
import boto.sqs
import boto.s3
import boto.sdb
from boto.s3.key import Key
from requests import get

from .simple import SimpleJobResultStore


class S3JobResultStore(SimpleJobResultStore):
    BASE_BUCKET = 'com.plotwatt.aws.results'

    def __init__(self, region, domain_name):
        s3_conn = boto.s3.connect_to_region(region)
        self.bucket = s3_conn.create_bucket(self.BASE_BUCKET)
        self.prefix = domain_name

    MANY_YEARS = 517672752

    def store(self, job_id, asset):
        import datetime

        timestr = datetime.date.today().strftime("%Y/%m/%d/%I")
        key = Key(self.bucket)
        key.key = self.prefix + '/' + timestr + str(job_id)
        key.set_contents_from_string(
            asset,
            headers={'Content-Type': 'text/plain'})
        key.set_canned_acl('private')
        return key.generate_url(expires_in=self.MANY_YEARS)

    def retrieve(self,output_url):
        return get(output_url)
