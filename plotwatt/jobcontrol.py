__author__ = 'willr'

import uuid
import datetime

import json


class Job(object):
    def __init__(self, job_id=None, status=None,
                 crt_time=None, cmd=None, output_url=None):

        self.cmd = cmd

        if crt_time is None:
            self.crt_time = datetime.datetime.utcnow().isoformat()
        else:
            self.crt_time = crt_time

        if job_id is None:
            self.job_id = str(uuid.uuid4())
        else:
            self.job_id = job_id

        if status is None:
            self.status = 'pending'
        else:
            self.status = status

        if output_url is None:
            self.output_url = ''
        else:
            self.output_url = output_url

    def to_json(self):
        return json.dumps({
            'job_id': self.job_id,
            'status': self.status,
            'cmd': self.cmd,
            'crt_time': self.crt_time,
            'output_url': self.output_url}
        )

    @staticmethod
    def from_dict(inp):
        if inp is None: return None
        return Job(job_id=inp.get('job_id', None), status=inp.get('status', None),
                   crt_time=inp.get('crt_time', None), cmd=inp.get('cmd', None),
                   output_url=inp.get('output_url', None))

    @staticmethod
    def from_json(input_str):
        return Job.from_dict(json.loads(input_str))


def parse_args():
    import argparse

    parser = argparse.ArgumentParser(
        description='Run,delete, kill and list jobs.')
    parser.add_argument('--list', help='List ALL the jobs.')
    parser.add_argument(
        '--list-running',
        help='List all the jobs currently running.')
    parser.add_argument(
        '--list-pending',
        help='List all the jobs currently pending.')
    parser.add_argument(
        '--list-killed',
        help='List all the jobs that have been killed.')
    parser.add_argument(
        '--list-finished',
        help='List all the jobs that have finished. ')
    parser.add_argument(
        '--list-failed',
        help='List all the jobs that have failed.')
    parser.add_argument(
        '--add',
        nargs='*',
        help='Add a command to the job queue.')
    parser.add_argument('--kill', help='Kill a job.')
    parser.add_argument('--rm', help='Remove a job.')
    return parser.parse_args()


class JobControl(object):
    def __init__(self, region, domain_name='plotwatt_jobs'):
        """
        @type domain_name: basestring
        @param domain_name: AWS base doc
        @type region: basestring
        @param region: AWS region ( us-east-1, us-west-2, etc. )
        """
        from .sqs import SQSMessageService
        from .sdb import SdbJobTracker

        # Acquire SQS and SimpleDB backed services
        self.message_service = SQSMessageService(region=region, domain_name=domain_name)
        self.tracking_service = SdbJobTracker(region=region, domain_name=domain_name)

    def register_job(self, cmd):
        job = Job(cmd=cmd)
        self.tracking_service.create_job(job)
        self.message_service.notify_job(job)
        return job

    def print_jobs(self):
        for r in self.tracking_service.list():
            print r.get('job_id') + r.get('cmd')

    def list_jobs(self):
        return self.tracking_service.list()

    def find_job(self, job_id):
        Job.from_dict(self.tracking_service.find(job_id))


def main():
    opts = parse_args()
    for opt in opts:
        print opt


if __name__ == '__main__':
    main()
