from boto.exception import SQSError

__author__ = 'willr'

import boto
import boto.sqs
from boto.sqs.message import Message
import boto.s3
import boto.sdb

import time
import logging
from .jobcontrol import Job

class SQSMessageService(object):
    RETRY_BASE_BACKOFF_TIME = 5
    RETRY_LIMIT = 10

    def __init__(self, region, domain_name):
        """
        @type region: basestring
        @param region: 
        @param domain_name:
        """
        sqs_conn = boto.sqs.connect_to_region(region)
        self.sqs_queue = sqs_conn.create_queue(domain_name)
        self.retry_attempts = self.RETRY_LIMIT
        self.retry_backoff_time = self.RETRY_BASE_BACKOFF_TIME

    def __iter__(self):

        """
        @type self: object
        @raise sqse: 
        """
        while True:
            try:
                messages = self.sqs_queue.get_messages(
                    num_messages=1,
                    visibility_timeout=40,
                    wait_time_seconds=20)
                if messages:
                    for msg in messages:
                        yield msg, Job.from_json(msg.get_body())
                self.retry_attempts = self.RETRY_LIMIT
                self.retry_backoff_time = self.RETRY_BASE_BACKOFF_TIME
            except SQSError as sqse:
                self.retry_attempts -= 1
                logging.info(
                    "SQS Exception:(%r). Retry: %d" %
                    (sqse, self.retry_attempts))

                if self.retry_attempts <= 0:
                    logging.error(
                        "SQS retries exceeded limit %d",
                        self.RETRY_LIMIT)
                    raise sqse
                else:
                    time.sleep(self.retry_backoff_time)
                    self.retry_backoff_time *= 1.2

    def task_accepted(self, msg):
        self.sqs_queue.delete_message(msg)

    def notify_job(self, job):
        """
        @param job:
        @return: @raise sqe:
        """
        msg = Message()
        msg.set_body(job.to_json())
        retry = 5
        while retry > 0:
            try:
                return self.sqs_queue.write(msg)
            except SQSError as sqe:
                logging.error(
                    "Error sending notification %s %r",
                    sqe.message,
                    sqe)
                retry -= 1
                time.sleep(2)

        if retry < 0: raise sqe
