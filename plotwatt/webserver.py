__author__ = 'willr'

from gevent.wsgi import WSGIServer
from webservice import app

import gevent
import signal

def run_forever():
    WSGIServer(('', 5000), app)

if __name__ == '__main__':
    gevent.signal(signal.SIGQUIT, gevent.shutdown)
    thread = gevent.spawn(run_forever)
    thread.join()