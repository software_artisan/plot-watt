__author__ = 'willr'

import logging
import time

import boto
import boto.ec2.autoscale
from boto.ec2.autoscale import LaunchConfiguration
from boto.ec2.autoscale import AutoScalingGroup
"""
 AWSTools in support of deploying ASG, checking instance health and managing infrastructure deployment
"""
class AWSTools(object):
    def __init__(self, region, asg_group_name='plot-watt-asg'):
        """
        @param region: AWS Region (us-east-1, etc.)
        @param asg_group_name:  Instance's grouped by
        """
        self.region = region
        self.launch_name = asg_group_name + "-config"
        self.asg_group = asg_group_name
        self.asg_conn = self.__get_asg_connection()
        self.ec2_conn = self.__get_ec2_connection()
        self.zones = self.__get_available_zones()

    def __get_asg_connection(self):
        retries = 5
        while retries > 0:
            conn = boto.ec2.autoscale.connect_to_region(self.region)
            if conn is None:
                retries -= 1
                time.sleep(5)
            else:
                return conn

        raise Exception("Couldn't create ASG connection.")

    def __get_ec2_connection(self):
        retries = 5
        while retries > 0:
            conn = boto.ec2.connect_to_region(self.region)
            if conn is None:
                retries -= 1
                time.sleep(5)
            else:
                return conn

        raise Exception("Couldn't create EC2 connection.")

    def __get_available_zones(self):
        zones = []
        for zone in self.ec2_conn.get_all_zones():
            if zone.state == 'available':
                zones.append(zone.name)
        return zones

    def __create_launch_config(self):

        lc = LaunchConfiguration(
            name=self.launch_name,
            instance_type='t1.micro',
            instance_profile_name='plot-watt-role',
            key_name='plot-watt',
            image_id='ami-fd8cb494',
            security_groups=['sg-9679bff3']
        )

        try:
            self.asg_conn.delete_launch_configuration(self.launch_name)
        except Exception:
            pass

        self.asg_conn.create_launch_configuration(lc)

        retries = 5
        while retries > 0:
            for lc in self.asg_conn.get_all_launch_configurations(names=[self.launch_name]):
                if lc.name == self.launch_name:
                    return lc
            time.sleep(5)
            retries -= 1

        raise Exception("Couldn't create launch configuration.")

    ASG_CHECK_TRIES = 30
    ASG_CHECK_TIMEOUT = 10

    def __create_asg_group(self):

        launch_config = self.asg_conn.get_all_launch_configurations(
            names=[self.launch_name])

        if launch_config is None or len(launch_config) == 0:
            raise Exception(
                'Launch configuration {} missing.'.format(self.launch_name))

        ag = AutoScalingGroup(
            group_name=self.asg_group,
            availability_zones=self.zones,
            launch_config=launch_config[0],
            min_size=3, max_size=3,
            connection=self.asg_conn)

        self.asg_conn.create_auto_scaling_group(ag)

        # Give ASG 5 minutes to pop up
        retries = self.ASG_CHECK_TRIES
        while retries >= 0:
            for group in self.asg_conn.get_all_groups():
                if group.name == self.asg_group:
                    return group
            logging.info(
                'Check {} for ASG {}'.format(
                    self.ASG_CHECK_TRIES -
                    retries))
            time.sleep(self.ASG_CHECK_TIMEOUT)
            retries -= 1

        logging.error(
            "Couldn't find ASG after {} seconds.".format(
                self.ASG_CHECK_TRIES *
                self.ASG_CHECK_TIMEOUT))

        raise Exception(
            "Couldn't find ASG after {} seconds.".format(
                self.ASG_CHECK_TRIES *
                self.ASG_CHECK_TIMEOUT))

    # Public

    def get_instance_addresses(self):
        """ Return the public host addresses of all the running instances in the ASG for software deployment"""
        group = self.asg_conn.get_all_groups(names=[self.asg_group])

        if group is None or len(group) == 0:
            raise Exception("Could't find ASG for {}".format(self.asg_group))

        addresses = []
        instance_ids = [i.instance_id for i in group[0].instances]
        instances = self.ec2_conn.get_only_instances(instance_ids)

        for instance in instances:
            addresses.append(instance.public_dns_name)

        return addresses

    def shutdown_asg(self):
        group = self.asg_conn.get_all_groups(names=[self.asg_group])
        group.shutdown_instances()

    def create_infrastructure(self):
        self.__create_launch_config()
        self.__create_asg_group()

def main():
    aws_region = 'us-east-1'
    asg_group_name = 'plot-watt-asg'

    aws = AWSTools(aws_region, asg_group_name)
    aws.create_infrastructure()


if __name__ == '__main__':
    main()
