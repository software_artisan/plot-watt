#!/usr/bin/env python

import sys
from .daemon import Daemon
from .service import service


class PlotWattService(Daemon):
    def run(self):
        service()

if __name__ == "__main__":
    daemon = PlotWattService(
        '/tmp/daemon-plot-watt.pid', stderr='/tmp/plotwatt_err.log',
        stdout='/tmp/plotwatt_out.log')
    if len(sys.argv) == 2:
        if 'start' == sys.argv[1]:
            daemon.start()
        elif 'stop' == sys.argv[1]:
            daemon.stop()
        elif 'restart' == sys.argv[1]:
            daemon.restart()
        else:
            print "Unknown command"
            sys.exit(2)
        sys.exit(0)
    else:
        print "usage: %s start|stop|restart" % sys.argv[0]
        sys.exit(2)
