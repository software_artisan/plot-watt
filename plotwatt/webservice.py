__author__ = 'willr'

#from plotwatt.plotwattapp import config
from flask import Flask, request, json, render_template, redirect, url_for, flash, _request_ctx_stack
from requests import *
from datetime import datetime as dt

import Queue

sns_message_queue = Queue.Queue()

app = Flask(__name__)

healthy = 'healthy',200

def fail_health_check(msg):
    healthy = msg,403

@app.route('/')
def health_check():
    return healthy

class SNSMessage(object):
    def __init__(self,payload):
        self.notification_id = payload[u'MessageId']
        self.timestamp = dt.strptime(payload[u'Timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ')
        self.unsubscribe_url = payload[u'UnsubscribeURL']
        self.topic = payload[u'TopicARN']
        self.msg_subject = payload[u'Subject']
        self.message = payload[u'Message']

def subscription_confirmation(payload):
    sns = SNSMessage(payload)

    resp = get(sns.confirmation_url) #Ping AWS URL to confirm SNS subscription
    if resp.status_code == 200:
        return 200
    else:
        fail_health_check('Could not confirm SNS subscription {} {} {}'.format(resp.status_code,sns.confirmation_url,sns.timestamp))
        return 403

def unsubscribe_confirmation(payload):
    return 200

def notification(payload):
    new_task_notification(SNSMessage(payload))

sns_actions = { 'SubscriptionConfirmation':subscription_confirmation,
                'UnsubscribeConfirmation':unsubscribe_confirmation,
                'Notification': notification }

@app.route('/sns', methods=['POST'])
def sns():
    payload = json.load(request.data)
    headers = request.headers
    arn = headers.get('x-amz-sns-subscription-arn')
    type = headers.get('x-amz-sns-message-type')

    resp_fcn = sns_actions[type]

    if resp_fcn is not None:
        return resp_fcn(payload)

    return '', 200

def new_task_notification(sns_message):
    sns_message_queue.put(sns_message_queue,False)



if __name__ == '__main__':


    # Switch this to false when deployed
    app.debug=True
    app.run(host='0.0.0.0',port=8090)

# Test

subscription_confirmation_msg = """{
  "Type" : "SubscriptionConfirmation",
  "MessageId" : "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
  "Token" : "2336412f37fb687f5d51e6e241d09c805a5a57b30d712f794cc5f6a988666d92768dd60a747ba6f3beb71854e285d6ad02428b09ceece29417f1f02d609c582afbacc99c583a916b9981dd2728f4ae6fdb82efd087cc3b7849e05798d2d2785c03b0879594eeac82c01f235d0e717736",
  "TopicArn" : "arn:aws:sns:us-east-1:123456789012:MyTopic",
  "Message" : "You have chosen to subscribe to the topic arn:aws:sns:us-east-1:123456789012:MyTopic.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
  "SubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-east-1:123456789012:MyTopic&Token=2336412f37fb687f5d51e6e241d09c805a5a57b30d712f794cc5f6a988666d92768dd60a747ba6f3beb71854e285d6ad02428b09ceece29417f1f02d609c582afbacc99c583a916b9981dd2728f4ae6fdb82efd087cc3b7849e05798d2d2785c03b0879594eeac82c01f235d0e717736",
  "Timestamp" : "2012-04-26T20:45:04.751Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLEpH+DcEwjAPg8O9mY8dReBSwksfg2S7WKQcikcNKWLQjwu6A4VbeS0QHVCkhRS7fUQvi2egU3N858fiTDN6bkkOxYDVrY0Ad8L10Hs3zH81mtnPk5uvvolIC1CXGu43obcgFxeL3khZl8IKvO61GWB6jI9b5+gLPoBc1Q=",
  "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
  }"""

notification_msg = """{
  "Type" : "SubscriptionConfirmation",
  "MessageId" : "165545c9-2a5c-472c-8df2-7ff2be2b3b1b",
  "Token" : "2336412f37fb687f5d51e6e241d09c805a5a57b30d712f794cc5f6a988666d92768dd60a747ba6f3beb71854e285d6ad02428b09ceece29417f1f02d609c582afbacc99c583a916b9981dd2728f4ae6fdb82efd087cc3b7849e05798d2d2785c03b0879594eeac82c01f235d0e717736",
  "TopicArn" : "arn:aws:sns:us-east-1:123456789012:MyTopic",
  "Message" : "You have chosen to subscribe to the topic arn:aws:sns:us-east-1:123456789012:MyTopic.\nTo confirm the subscription, visit the SubscribeURL included in this message.",
  "SubscribeURL" : "https://sns.us-east-1.amazonaws.com/?Action=ConfirmSubscription&TopicArn=arn:aws:sns:us-east-1:123456789012:MyTopic&Token=2336412f37fb687f5d51e6e241d09c805a5a57b30d712f794cc5f6a988666d92768dd60a747ba6f3beb71854e285d6ad02428b09ceece29417f1f02d609c582afbacc99c583a916b9981dd2728f4ae6fdb82efd087cc3b7849e05798d2d2785c03b0879594eeac82c01f235d0e717736",
  "Timestamp" : "2012-04-26T20:45:04.751Z",
  "SignatureVersion" : "1",
  "Signature" : "EXAMPLEpH+DcEwjAPg8O9mY8dReBSwksfg2S7WKQcikcNKWLQjwu6A4VbeS0QHVCkhRS7fUQvi2egU3N858fiTDN6bkkOxYDVrY0Ad8L10Hs3zH81mtnPk5uvvolIC1CXGu43obcgFxeL3khZl8IKvO61GWB6jI9b5+gLPoBc1Q=",
  "SigningCertURL" : "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-f3ecfb7224c7233fe7bb5f59f96de52f.pem"
  }
"""