__author__ = 'willr'

from boto.exception import SDBResponseError
import boto
import boto.sdb
import logging

class SdbJobTracker(object):
    def __init__(self, region='us-east-1', domain_name='job_track'):
        self.region = region
        self.domain_name = domain_name
        self.sdb_conn = boto.sdb.connect_to_region(region)
        self.domain = self.sdb_conn.create_domain(domain_name)

    def current_status(self, job_id):
        rec = self.sdb_conn.get_attributes(
            domain_or_name=self.domain,
            item_name=job_id,
            attribute_names='status',
            consistent_read=True)
        if rec is None:
            return None
        return rec.get('status',None)

    def can_start(self,job_id):
        return self.current_status(job_id) == 'pending'

    def delete_all(self):
        self.sdb_conn.delete_domain(self.domain_name)
        self.sdb_conn.create_domain(self.domain_name)

    def list(self, status=None):
        if status is None:
            return self.domain.select(
                query='select * from {} where crt_time is not null order by crt_time asc'.format(
                    self.domain_name),
                consistent_read=True,
                max_items=50)
        return self.domain.select(
            query='select * from {} where status={} and crt_time is not null order by crt_time asc'.format(
                self.domain_name,
                status),
            consistent_read=True,
            max_items=50)

    def find(self,job_id):
        return self.domain.get_item(job_id)

    def list_killed(self):
        return self.list('killed')

    def list_running(self):
        return self.list('running')

    def list_finished(self):
        return self.list('finished')

    def toggle_status(self, job_id, expected_status, new_status):
        try:
            if self.sdb_conn.put_attributes(
                    self.domain, job_id, {'status': new_status}, replace=True,
                    expected_value=['status', expected_status]):
                return True
        except boto.exception.SDBResponseError as e:
            if e.status != 404 and e.status != 409:
                raise e

    def update_status(self, job_id, location=None,status_code=None,output_url=None):
        attrs = {}
        if location is not None:
            attrs.update({'location':location})
        if status_code is not None:
            attrs.update({'status_code':status_code})
        if output_url is not None:
            attrs.update({'output_url':output_url})

        if len(attrs) > 0:
            try:
                self.sdb_conn.put_attributes(self.domain, job_id, attrs)
            except boto.exception.SDBResponseError as e:
                if e.status != 404 and e.status != 409:
                    raise e

    def start_job(self, job_id,location):
        self.update_status(job_id,location=location)
        return self.toggle_status(job_id,expected_status='pending',new_status='running')

    def fail_job(self,job_id,status_code,output_url):
        self.update_status(job_id,status_code=status_code,output_url=output_url)
        return self.toggle_status(job_id,expected_status='running',new_status='failed')

    def kill_job(self, job_id):
        return self.toggle_status(job_id,expected_status='running',new_status='killed')

    def finish_job(self,job_id,status_code,output_url):
        self.update_status(job_id,status_code=status_code,output_url=output_url)
        return self.toggle_status(job_id,expected_status='running',new_status='finished')

    def create_job(self, job):

        attrs = {'job_id': job.job_id, 'status': job.status, 'cmd': job.cmd, 'crt_time': job.crt_time}

        logging.debug(attrs)

        try:
            self.domain.put_attributes(job.job_id,attrs,replace=False)
        except boto.exception.SDBResponseError as e:
            if e.status != 404 and e.status != 409:
                raise e
